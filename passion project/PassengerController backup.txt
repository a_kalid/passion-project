
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MvcRides.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace MvcRides.Controllers
{
    public class PassengerController : Controller
    {
        private RideCMSContext db = new RideCMSContext();

        // GET: Passenger
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult List()
        {

            return View(db.Passengers.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }

        [HttpPost]
        public ActionResult Create(string FirstName_New, string LastName_New, int Phone_New, string Email_New)
        {
            //actually ask to put info into db

            string query = "insert into passengers (firstname, lastname, phone, email) values (@firstname, @lastname, @phone, @email)";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@firstname", FirstName_New);
            myparams[1] = new SqlParameter("@lastname", LastName_New);
            myparams[2] = new SqlParameter("@phone", Phone_New);
            myparams[3] = new SqlParameter("@email", Email_New);

            db.Database.ExecuteSqlCommand(query);

            return RedirectToAction("List");
        }
    }
}