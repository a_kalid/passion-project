﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.SqlClient;
using System.Data.Entity;
using System.Net;
using System.Web;
using System.Web.Mvc;
using CarRentalSystem.Models;
using System.Diagnostics;

namespace CarRentalSystem.Controllers
{
    public class OrderController : Controller
    {
        CarCMSContext db = new CarCMSContext();
       

        public ActionResult List()
        {

            //var orders = db.Orders.Include(o => o.Car).Include(o => o.Customer);
            var orders = db.Orders.ToList();
            return View(orders.ToList());


        }

        // GET: Orders
        public ActionResult Index()
        {
            return RedirectToAction("List");
        }

        // GET: Orders/Create
        public ActionResult Create()
        {
            return View();
        }


        // POST: Orders/Create

        [HttpPost]
        public ActionResult Create(string order_title_new, string order_date_new)
        {

            //check to see if we have these values
            Debug.WriteLine(order_title_new + order_date_new);

            string query = "insert into orders (ordertitle, orderdate) values (@title, @date)";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@title", order_title_new);
            myparams[1] = new SqlParameter("@date", order_date_new);

            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");

        }


        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from orders where OrderID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Order myorder = db.Orders.SqlQuery(query, param).FirstOrDefault();
            return View(myorder);
        }

        // POST: Orders/Edit/5

        [HttpPost]

        public ActionResult Edit(int id, string ordertitle, string orderdate)
        {

            if ((id == null) || (db.Orders.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update orders set ordertitle=@title, orderdate=@date where OrderID = @id";
            SqlParameter[] myparams = new SqlParameter[3];

            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@title", ordertitle);
            myparams[2] = new SqlParameter("@date", orderdate);

            //db.Database.ExecuteSqlCommand(query, myparams);
            Debug.WriteLine(query);

            return RedirectToAction("Details/" + id);
        }

        public ActionResult Details (int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from orders where OrderID = @id";
            SqlParameter param = new SqlParameter("@id", id);
            Order ordertodetails = db.Orders.Find(id);

            return View(ordertodetails);

        }

        // GET: Orders/Delete/5
        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Orders.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from Orders where OrderID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);

            return RedirectToAction("List");

        }

    }
}

