﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using MVCmovies.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace MVCmovies.Controllers
{
    public class MovieController : Controller
    {
        private MovieCMSContext db = new MovieCMSContext();
        // GET: Movie
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {

            return View(db.Movies.ToList());
        }
        public ActionResult New()
        {
            //no information needed from db

            return View();
        }


        // Create new movie
        [HttpPost]
        public ActionResult Create(string Title, DateTime ReleaseDate, string Genre, string Price)
        {
            //actually ask to put info into db

            string query = "insert into drivers (Title, ReleaseDate, Genre, Price) values (@Title, @ReleaseDate, @Genre, @Price)";
            SqlParameter[] myparams = new SqlParameter[4];
            myparams[0] = new SqlParameter("@Title", Title);
            myparams[1] = new SqlParameter("@ReleaseDate", ReleaseDate);
            myparams[2] = new SqlParameter("@Genre", Genre);
            myparams[3] = new SqlParameter("@Price", Price);


            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
    }
}