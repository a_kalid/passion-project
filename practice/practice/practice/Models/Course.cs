﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace practice.Models
{
    public class Course
    {
        [Key]
        public int CourseID { get; set; }


        public string Description { get; set; }


        

        public virtual ICollection<Student> Students { get; set; }
    }
}