﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace practice.Models
{
    public class PracticeCMSContext : DbContext
    {
            public PracticeCMSContext()
            {

            }
            public DbSet<Student> Students { get; set; }
            public DbSet<Course> Courses { get; set; }

        
    }
}
