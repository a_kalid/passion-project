﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace practice.Models
{
    public class Student
    {
        
        [Key]
        public int StudentID { get; set; }
        public string S_Name { get; set; }
        public string S_Email { get; set; }
        
        public Course Course { get; set; }


    }

}
    
