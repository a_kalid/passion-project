﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using practice.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;

namespace practice.Controllers
{
    

    public class CourseController : Controller
    {
        private PracticeCMSContext db = new PracticeCMSContext();
        // GET: Course
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {

            return View(db.Courses.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }
    }
}