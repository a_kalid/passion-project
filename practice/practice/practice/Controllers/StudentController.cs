﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using practice.Models;
using System.Diagnostics;
using System.Data.SqlClient;
using System.IO;


namespace practice.Controllers
{

    public class StudentController : Controller
    {
        private PracticeCMSContext db = new PracticeCMSContext();
        // GET: Student
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult List()
        {

            return View(db.Students.ToList());
        }

        public ActionResult New()
        {
            //no information needed from db

            return View();
        }
        // Create new student

        [HttpPost]
        public ActionResult Create(string S_Name, string S_Email)
        {
            //actually ask to put info into db

            string query = "insert into students (S_Name, S_Email) values (@S_Name, @S_Email )";
            SqlParameter[] myparams = new SqlParameter[2];
            myparams[0] = new SqlParameter("@S_Name", S_Name);
            myparams[1] = new SqlParameter("@S_Email", S_Email);


            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");

        }
        // GET: Student/Edit/5
        public ActionResult Edit(int? id)
        {
            Debug.WriteLine("testing student edit");
            if ((id == null) || (db.Students.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "select * from students where StudentID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            Student mystudent = db.Students.SqlQuery(query, param).FirstOrDefault();
            return View(mystudent);
        }

        
        // POST: Students/Edit/5

        [HttpPost]

        public ActionResult Edit(int id, string S_Name, string S_Email)
        {

            if ((id == null) || (db.Students.Find(id)) == null)
            {
                return HttpNotFound();
            }

            string query = "update students set S_Name=@S_Name, S_Email=@S_Email  where StudentID = @id";
            SqlParameter[] myparams = new SqlParameter[3];
            myparams[0] = new SqlParameter("@id", id);
            myparams[1] = new SqlParameter("@S_Name", S_Name);
            myparams[2] = new SqlParameter("@S_Email", S_Email);
           
            db.Database.ExecuteSqlCommand(query, myparams);

            return RedirectToAction("List");
        }
        //delete Student
        // GET: Students/Delete


        public ActionResult Delete(int? id)
        {
            if ((id == null) || (db.Students.Find(id) == null))
            {
                return HttpNotFound();
            }
            string query = "delete from Students where StudentID=@id";
            SqlParameter param = new SqlParameter("@id", id);
            db.Database.ExecuteSqlCommand(query, param);
            return RedirectToAction("List");

        }

    }
}